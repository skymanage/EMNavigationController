//
//  EMTabViewController.m
//  EMNavigationController
//
//  Created by EasonWang on 13-11-25.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//


#define viewController @"viewController"
#define tabTitle @"title"
#define imageDefault @"imageDefault"
#define imageSelected @"imageSelected"


#import "EMTabViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"

@implementation EMTabViewController


/**
 在此方法中设置tabbar切换的viewController
 请严格按照规定的格式来配置
 此tab按钮包含默认图片 imageDefault 和选中图片 imageSelected 请对照相关 key-value来配置
 */

+(NSArray *)getTabViewControllers
{
    NSArray* tabVCArray = nil;
    
    FirstViewController *first = [[FirstViewController alloc]initWithNibName:@"FirstViewController" bundle:nil];
    SecondViewController *second = [[SecondViewController alloc]initWithNibName:@"SecondViewController" bundle:nil];
    
    tabVCArray = @[
                   @{
                       viewController       :// controller
                       first,
                       tabTitle             :// title
                       @"首页",
                       imageDefault         :// the default image
                       @"main@2x.png",
                       imageSelected        :// the selected image
                       @"mainClick@2x.png"
                       },
                   @{
                       viewController       :// controller
                       second,
                       tabTitle             :// title
                       @"second",
                       imageDefault         :// the default image
                       @"myNear@2x.png",
                       imageSelected        :// the selected image
                       @"myNearClick@2x.png"
                       }
                   ];
    return tabVCArray;
}

@end
