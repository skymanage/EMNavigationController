//
//  AutoDismissAlertView.h
//  LLYG
//
//  Created by EasonWang on 13-9-24.
//  Copyright (c) 2013年 ShiTengTechnology. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AutoDismissAlertView : UIAlertView {
    NSUInteger mDelaySeconds;
    BOOL mAnimated;
    id<UIAlertViewDelegate> mDelegate;
}

-(id) initWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate dismissAfterSeconds:(NSUInteger)seconds animated:(BOOL)animated;

-(void) dismiss;

@end
